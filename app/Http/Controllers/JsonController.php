<?php

namespace App\Http\Controllers;

use App\Sby;
use Illuminate\Http\Request;

class JsonController extends Controller
{
    public function index(Request $request){
        if ($request->input('kelurahan') == null && $request->input('kecamatan') == null){
            $datas = Sby::select('*')
                ->selectRaw('asText(SHAPE) as geometry')
                ->get();
        }
        elseif($request->input('kelurahan') != null && ($request->input('kecamatan') == null)) {
            $kelurahan = explode(",",$request->input('kelurahan'));
            $datas = Sby::select('*')
                ->whereIn('name',$kelurahan)
                ->selectRaw('asText(SHAPE) as geometry')
                ->get();
        }
        elseif($request->input('kelurahan') == null && ($request->input('kecamatan') != null)){
            $kecamatan = explode(",",$request->input('kecamatan'));
            $datas = Sby::select('*')
                ->whereIn('kecamatan',$kecamatan)
                ->selectRaw('asText(SHAPE) as geometry')
                ->get();
        }
        else{
            return response()->json('pilih kecamatan atau kelurahan saja');
        }

        $data2 = [
            'type'=> 'FeatureCollection',
            'name'=> 'Shape of Surabaya',
            'crs' => [],
            'features' =>[],
        ];
        $data2['crs']=[
            'type' => 'name',
            'properties' =>[
                'supported by' => 'https://apollo16team.com',
                'project by' => 'kojyou-project'
            ],
        ];
        foreach ($datas as $key=> $data1) {
            $str_replace=str_replace([',',' ','(((',')))','((','))','MULTIPOLYGON','POLYGON'], ['],[',',','[[[[',']]]]','[[[[',']]]]','',''], $data1['geometry']);
            $geojson = str_replace([']]]]],[[[[['],[']]],[[['],$str_replace);

            $data2['features'][$key] = [
                'type' => 'Feature',
                'properties'=>[
                    'Name'=>$data1->name,
                    'Kecamatan'=>$data1->kecamatan,
                    'Desa'=>$data1->desa,
                    'origin geometry' => $data1['geometry'],
                ],
                'geometry'=>[
                    'type'=> 'MultiPolygon',
                    'coordinates'=>json_decode($geojson),
                ],
            ];
        }
        return response()->json($data2);

    }

    public function distance(Request $request){
        $lat=$request->input('lat');
        $long=$request->input('long');

        $hereami = 'POINT('.$long.', '.$lat.')';

        $datas=Sby::select('*')
            ->selectRaw('asText(SHAPE) as geometry')
            ->whereRaw('ST_Distance('.$hereami.', SHAPE) < 0.001')
            ->get();

        $data2 = [
            'type'=> 'FeatureCollection',
            'name'=> 'Shape of Surabaya',
            'crs' => [],
            'features' =>[],
        ];
        $data2['crs']=[
            'type' => 'name',
            'properties' =>[
                'supported by' => 'https://apollo16team.com',
                'project by' => 'kojyou-project'
            ],
        ];

        foreach ($datas as $key=> $data1) {
            $str_replace=str_replace([',',' ','(((',')))','((','))','MULTIPOLYGON','POLYGON'], ['],[',',','[[[[',']]]]','[[[[',']]]]','',''], $data1['geometry']);
            $geojson = str_replace([']]]]],[[[[['],[']]],[[['],$str_replace);
            $key=-1;
            $data2['features'][$key] = [
                'type' => 'Feature',
                'properties'=>[
                    'Name'=>$data1->name,
                    'Kecamatan'=>$data1->kecamatan,
                    'Desa'=>$data1->desa,
                    'origin geometry' => $data1['geometry'],
                ],
                'geometry'=>[
                    'type'=> 'MultiPolygon',
                    'coordinates'=>json_decode($geojson),
                ],
            ];
        }
        $data2['features'][$key+1] = [
            'type' => 'Feature',
            'properties'=>[
                'Name'=>'Coordinates'
            ],
            'geometry'=>[
                'type'=>'Point',
                'coordinates'=>[$long,$lat]
            ],
        ];
        return response()->json($data2);

    }
}
